import AddTask from "@/components/AddTask";
import TodoList from "@/components/TodoList";

export default function Home() {
  return (
    <main className="max-h-full max-w-full p-4 bg-gray-800">
      <div className="text-center mt-2 bg-gray-900 p-6 rounded-lg shadow-md">
        <h1 className="text-4xl font-extrabold text-indigo-400 mb-8">
          Todo List App
        </h1>
        <AddTask />
        <TodoList />
      </div>
    </main>
  );
}
