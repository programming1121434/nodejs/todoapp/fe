"use client";
import { AiOutlinePlus } from "react-icons/ai";
import Modal from "./modal";
import { useState } from "react";
// import { useRouter } from "next/navigation";

const AddTask = () => {
  // const router = useRouter();
  const [modalOpen, setModalOpen] = useState(false);
  const [newTaskValue, setNewTaskValue] = useState("");
  const [newTaskDesc, setNewTaskDesc] = useState("");
  const [newDueDate, setNewDueDate] = useState("");

  const handleSubmitNewTodo = async (e) => {
    e.preventDefault();
    const req = await fetch("https://todo-be-kanomnutt.azurewebsites.net/api/todo", {
      method: "POST",
      mode: "no-cors",
      body: JSON.stringify({ 
        text: newTaskValue, 
        description: newTaskDesc, 
        duedate: newDueDate 
      }),
      headers: {
        "Access-Control-Allow-Origin": "https://todo-be-kanomnutt.azurewebsites.net",
        "Content-Type": "application/json",
      },
    });
    setNewTaskValue("");
    setNewTaskDesc("");
    setModalOpen(false);
    // router.refresh();
    window.location.reload(true);
  };

  return (
    <div className="mb-4">
      <button
        className="btn btn-primary w-full"
        onClick={() => setModalOpen(true)}
      >
        Add New Task <AiOutlinePlus className="ml-1" />
      </button>
      <Modal modalOpen={modalOpen} setModalOpen={setModalOpen}>
        <form onSubmit={handleSubmitNewTodo}>
          <h3 className="font-bold text-lg">Add new Task</h3>
          <div className="modal-action flex flex-col space-y-4">
            <div className="flex flex-col w-full">
              <input
                type="text"
                placeholder="Task Name"
                className="input input-bordered input-primary"
                value={newTaskValue}
                onChange={(e) => setNewTaskValue(e.target.value)}
              />
              <textarea
                className="textarea textarea-bordered textarea-primary textarea-sm mt-4"
                placeholder="Task Description"
                value={newTaskDesc}
                onChange={(e) => setNewTaskDesc(e.target.value)}
              />
              <h2 className="font-semibold text-base mt-4">Due Date</h2>
              <input 
                type="date" 
                placeholder="Due Date" 
                className="input input-bordered input-primary mt-4"
                value={newDueDate}
                onChange={(e) => setNewDueDate(e.target.value)}/>
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </div>
        </form>
      </Modal>
    </div>
  );
};

export default AddTask;
