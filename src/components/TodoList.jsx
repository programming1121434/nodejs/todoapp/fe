"use client";
import { useState, useEffect } from "react";
import { FiEdit, FiTrash2 } from "react-icons/fi";
import Modal from "./modal";
import { useRouter } from "next/navigation";

const TodoList = () => {
  const [tasksValues, setTasksValues] = useState([]);
  const [openModalEdit, setOpenModalEdit] = useState(false);
  const [taskToEdit, setTaskToEdit] = useState();
  const [tasktoEditDesc, settasktoEditDesc] = useState();
  const [editingTaskId, setEditingTaskId] = useState(null);
  const [deleteTaskId, setDeleteTaskId] = useState(null);
  const [taskToDelete, setTaskToDelete] = useState();
  const [taskDueDate, setTaskDueDate] = useState();
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const router = useRouter();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("https://todo-be-kanomnutt.azurewebsites.net/api/todo", {
          method: "GET",
          cache: "no-store",
        });
        const data = await response.json();
        setTasksValues(data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, []);

  const handleSubmitEditTodo = async (e) => {
    e.preventDefault();
    const response = await fetch("https://todo-be-kanomnutt.azurewebsites.net/api/todo", {
      method: "PUT",
      cache: "no-store",
      body: JSON.stringify({
        id: editingTaskId,
        text: taskToEdit,
        description: tasktoEditDesc,
        duedate: taskDueDate,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });
    setOpenModalEdit(false);
    router.refresh();
    window.location.reload(true);
  };

  const toggleTodo = async (taskId, completed) => {
    const response = await fetch(`https://todo-be-kanomnutt.azurewebsites.net/api/todo/${taskId}`, {
      method: "POST",
      mode:'no-cors',
      cache: "no-store",
      body: JSON.stringify({
        id: taskId,
        completed: !completed,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });
    window.location.reload(true);
  };

  const handleSubmitDeleteTodo = async (e) => {
    const response = await fetch("https://todo-be-kanomnutt.azurewebsites.net/api/todo", {
      method: "DELETE",
      body: JSON.stringify({
        id: deleteTaskId,
        text: taskToDelete,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });
    setOpenModalDelete(false);
    router.refresh();
    window.location.reload(true);
  };


  return (
    <div className="overflow-x-auto min-h-screen text-gray-200">
      <table className="table">
        <thead>
          <tr>
            <th className="py-2">TASK</th>
            <th className="py-2">DESCRIPTION</th>
            <th className="py-2">Created</th>
            <th className="py-2">Due Date</th>
            <th className="py-2">ACTIONS</th>
            <th className="py-2">STATUS</th>
          </tr>
        </thead>
        <tbody>
          {tasksValues.map((task) => (
            <tr key={task._id} className="hover">
              <td className="w-1/4 py-3">{task.text}</td>
              <td className="w-1/3 py-3">{task.description}</td>
              <td className="w-1/7 py-3">{task.createdDate}</td>
              <td className="w-1/7 py-3">{task.duedate}</td>
              <td className="flex gap-2" colSpan="3">
              <input
                type="checkbox"
                className="checkbox checkbox-success checkbox-md"
                checked={task.completed}
                onChange={() => toggleTodo(task._id, task.completed)}
              />
              <FiEdit
                className="text-blue-500 cursor-pointer"
                size={25}
                onClick={() => {
                  setOpenModalEdit(true),
                  setTaskToEdit(task.text),
                  setEditingTaskId(task._id),
                  settasktoEditDesc(task.description),
                  setTaskDueDate(task.duedate)
                }}
              />
              <Modal
                modalOpen={openModalEdit}
                setModalOpen={setOpenModalEdit}
              >
                <form onSubmit={handleSubmitEditTodo}>
                  <h3 className="font-bold text-lg text-center">Edit task</h3>
                  <div className="modal-action flex flex-col space-y-4">
                    <div className="flex flex-col w-full">
                      <input
                        value={taskToEdit}
                        onChange={(e) => setTaskToEdit(e.target.value)}
                        type="text"
                        placeholder="Type here"
                        className="input input-bordered input-primary"
                      />
                      <textarea
                        className="textarea textarea-bordered textarea-primary textarea-sm mt-4"
                        placeholder="Task Description"
                        value={tasktoEditDesc}
                        onChange={(e) => settasktoEditDesc(e.target.value)}
                      />
                      <h3 className="font-semibold text-base mt-4 text-center">Due Date</h3>
                      <input 
                      value={taskDueDate}
                      onChange={(e) => setTaskDueDate(e.target.value)}
                      type="date"
                      className="input input-bordered input-primary mt-4"
                      />
                      <button type="submit" className="btn mt-4">
                        Submit
                      </button>
                    </div>
                  </div>
                </form>
              </Modal>

              <FiTrash2
                className="text-red-500 cursor-pointer"
                size={25}
                onClick={() => {
                  setOpenModalDelete(true),
                    setTaskToDelete(task.text),
                    setDeleteTaskId(task._id);
                }}
              />
                <Modal
                  modalOpen={openModalDelete}
                  setModalOpen={setOpenModalDelete}
                >
                  <h3 className="text-lg">
                    Are you sure you want to delete this task?
                  </h3>
                  <p className="text-gray-600 mt-2 text-base">
                    Task to delete: {taskToDelete}
                  </p>
                  <div className="modal-action">
                    <button
                      onClick={() => handleSubmitDeleteTodo(task.id)}
                      className="btn"
                    >
                      Yes
                    </button>
                  </div>
                </Modal>
              </td>
              <td className="w-1/7 py-3">
                {task.completed ? "Completed" : "Not Completed"}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default TodoList;
