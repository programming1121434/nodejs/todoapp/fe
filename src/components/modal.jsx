const Modal = ({ modalOpen, setModalOpen, children }) => (
  <div className={`modal ${modalOpen ? "modal-open" : ""}`}>
    <div className="modal-box relative p-6">
      <label
        onClick={() => setModalOpen(false)}
        className="btn btn-sm btn-circle absolute right-2 top-2"
      >
        x
      </label>
      {children}
    </div>
  </div>
);

export default Modal;
